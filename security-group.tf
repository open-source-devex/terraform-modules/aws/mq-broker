locals {
  security_group_ingres_port_rule          = formatlist("%s-%s-tcp", lower(var.engine_type), var.security_group_ingress_ports)
  security_group_ingress_cidr_block_tuples = setproduct(local.security_group_ingres_port_rule, var.security_group_ingress_cidr_blocks)
  security_group_ingress_cidr_blocks = [for tuple in local.security_group_ingress_cidr_block_tuples : {
    rule = tuple[0], cidr_blocks = tuple[1]
  }]
  security_group_ingress_src_security_group_ids_tuples = setproduct(local.security_group_ingres_port_rule, var.security_group_ingress_src_security_group_ids)
  security_group_ingress_src_security_group_ids = [for tuple in local.security_group_ingress_src_security_group_ids_tuples : {
    rule = tuple[0], source_security_group_id = tuple[1]
  }]
}

module "broker_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.2.0"

  create = var.create_broker_security_group

  name        = "mqbroker-${local.name}"
  description = "Security group for ActiveMQ Broker ${local.name}"
  vpc_id      = var.vpc_id

  tags = local.tags

  ingress_with_cidr_blocks              = local.security_group_ingress_cidr_blocks
  ingress_with_source_security_group_id = local.security_group_ingress_src_security_group_ids

  rules = {
    activemq-5671-tcp  = [5671, 5671, "tcp", "ActiveMQ AMQP"]
    activemq-8162-tcp  = [8162, 8162, "tcp", "ActiveMQ Web Interface"]
    activemq-8883-tcp  = [8883, 8883, "tcp", "ActiveMQ MQTT"]
    activemq-61614-tcp = [61614, 61614, "tcp", "ActiveMQ STOMP"]
    activemq-61617-tcp = [61617, 61617, "tcp", "ActiveMQ OpenWire"]
    activemq-61619-tcp = [61619, 61619, "tcp", "ActiveMQ WebSocket"]
  }
}
