# mq-broker

Terraform module to provision and manage AWS MQ Broker.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| apply\_immediately | Specifies whether any broker modifications are applied immediately, or during the next maintenance window. | bool | `"false"` | no |
| auto\_minor\_version\_upgrade | Enables automatic upgrades to new minor versions for brokers, as Apache releases the versions. | bool | `"true"` | no |
| broker\_xml\_configuration | The broker configuration in XML format. See official docs for supported parameters and format of the XML. | string | `"null"` | no |
| create\_broker\_security\_group | Whether to create a security group for the broker, used when `var.create_broker_security_group = true` | bool | `"true"` | no |
| deployment\_mode | The deployment mode of the broker. Supported: SINGLE_INSTANCE and ACTIVE_STANDBY_MULTI_AZ | string | `"SINGLE_INSTANCE"` | no |
| enable\_audit\_logging | Logging configuration of the broker. Enables audit logging. User management action made using JMX or the ActiveMQ Web Console is logged. | bool | `"false"` | no |
| enable\_general\_logging | Logging configuration of the broker. Enables general logging via CloudWatch. | bool | `"false"` | no |
| engine\_type | The type of broker engine | string | `"ACTIVEMQ"` | no |
| engine\_version | The version of the broker engine. See the AmazonMQ Broker Engine docs for supported versions | string | `"5.15.9"` | no |
| host\_instance\_type | The broker's instance type. e.g. mq.t2.micro or mq.m4.large | string | `"mq.t2.micro"` | no |
| maintenance\_day\_of\_week | Maintenance window start time. The day of the week. e.g. MONDAY, TUESDAY, or WEDNESDAY | string | n/a | yes |
| maintenance\_time\_of\_day | Maintenance window start time. The time, in 24-hour format. e.g. 02:00 | string | n/a | yes |
| maintenance\_time\_zone | Maintenance window start time. The time zone, UTC by default, in either the Country/City format, or the UTC offset format. e.g. CET | string | `"UTC"` | no |
| name | The name to be given to the broker and other resources, leave emply to have a default name generated | string | `"null"` | no |
| publicly\_accessible | Whether to enable connections from applications outside of the VPC that hosts the broker's subnets. | bool | `"false"` | no |
| security\_group\_ingress\_cidr\_blocks | CIDR blocks from where ingress traffic to the broker is allowed, used when `var.create_broker_security_group = true` | list(string) | `[]` | no |
| security\_group\_ingress\_ports | Ports to where ingress traffic to the broker is allowed, used when `var.create_broker_security_group = true` | list(number) | `[ 5671, 8883, 61614, 61617, 61619 ]` | no |
| security\_group\_ingress\_src\_security\_group\_ids | Security Group IDs from where ingress traffic to the broker is allowed, used when `var.create_broker_security_group = true` | list(string) | `[]` | no |
| security\_groups | List of security group ids to assign to broker, used when `var.create_broker_security_group = false` | list(string) | `[]` | no |
| tags | Tags to be added to resources that support it | map(string) | `{}` | no |
| users | Users to be configured in the broker, at least one admin user needs to be configured | object | `{}` | no |
| vpc\_id | The id of the vpc where the broker is deployed | string | n/a | yes |
| vpc\_subnet\_ids | The list of subnet IDs in which to launch the broker. A SINGLE_INSTANCE deployment requires one subnet. An ACTIVE_STANDBY_MULTI_AZ deployment requires two subnets. | list(string) | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| mq\_broker\_console\_urls | List of ActiveMQ Broker Web Console URLs |
| mq\_broker\_endpoints | List of ActiveMQ Broker Endpoints |
| mq\_broker\_endpoints\_amqp | List of ActiveMQ Broker AMQP Endpoints |
| mq\_broker\_endpoints\_mqtt | List of ActiveMQ Broker MQTT Endpoints |
| mq\_broker\_endpoints\_openwire | List of ActiveMQ Broker OpenWire Endpoints |
| mq\_broker\_endpoints\_stomp | List of ActiveMQ Broker Stomp Endpoints |
| mq\_broker\_endpoints\_wss | List of ActiveMQ Broker WSS Endpoints |
