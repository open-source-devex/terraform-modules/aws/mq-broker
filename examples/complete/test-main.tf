terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-mq-broker-complete"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

module "activemq" {
  source = "../.."

  project     = "open-source-devex"
  environment = "test"

  name = "foobar"

  engine_version = "5.15.8"

  vpc_id                       = module.vpc.vpc_id
  vpc_subnet_ids               = module.vpc.private_subnets
  create_broker_security_group = false
  security_groups              = [module.test_broker_sg.this_security_group_id]

  host_instance_type = "mq.t2.micro"

  apply_immediately          = true
  auto_minor_version_upgrade = false
  publicly_accessible        = true

  enable_general_logging = true
  enable_audit_logging   = true

  maintenance_day_of_week = "MONDAY"
  maintenance_time_of_day = "02:00"
  maintenance_time_zone   = "CET"

  deployment_mode = "ACTIVE_STANDBY_MULTI_AZ"

  users = {
    admin = {
      password       = "foobar1234567890"
      groups         = ["admin"]
      console_access = true
    }

    some_user = {
      password       = "1234567890foobar"
      groups         = []
      console_access = false
    }
  }

  broker_xml_configuration = <<DATA
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<broker xmlns="http://activemq.apache.org/schema/core">
  <plugins>
    <forcePersistencyModeBrokerPlugin persistenceFlag="true"/>
    <statisticsBrokerPlugin/>
    <timeStampingBrokerPlugin ttlCeiling="86400000" zeroExpirationOverride="86400000"/>
  </plugins>
</broker>
DATA
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "simple-example"

  cidr = "10.0.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  enable_nat_gateway = false
}

module "test_broker_sg" {
  source  = "terraform-aws-modules/security-group/aws//modules/activemq"
  version = "3.2.0"

  name        = "Test ActiveMQ sec group"
  description = "Security group for ActiveMQ Broker"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["10.0.0.0/16"]
  egress_cidr_blocks  = ["10.0.0.0/16"]
}
