terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-mq-broker-simple"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

module "activemq" {
  source = "../.."

  vpc_id                  = module.vpc.vpc_id
  vpc_subnet_ids          = module.vpc.private_subnets
  maintenance_day_of_week = "MONDAY"
  maintenance_time_of_day = "02:00"


  users = {
    admin = {
      password       = "foobar1234567890"
      groups         = ["admin"]
      console_access = true
    }
  }

  security_group_ingress_cidr_blocks = ["10.0.0.0/16"]
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "simple-example"

  cidr = "10.0.0.0/16"

  azs             = ["eu-west-1a"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]

  enable_nat_gateway = false
}
