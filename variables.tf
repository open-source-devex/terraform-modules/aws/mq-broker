variable project {
  type    = string
  default = "prj"
}

variable environment {
  type    = string
  default = "env"
}

variable "tags" {
  description = "Tags to be added to resources that support it"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "The id of the vpc where the broker is deployed"
  type        = string
}

variable "vpc_subnet_ids" {
  description = "The list of subnet IDs in which to launch the broker. A SINGLE_INSTANCE deployment requires one subnet. An ACTIVE_STANDBY_MULTI_AZ deployment requires two subnets."
  type        = list(string)
}

variable "broker_xml_configuration" {
  description = "The broker configuration in XML format. See official docs for supported parameters and format of the XML."
  type        = string
  default     = null
}

variable "name" {
  description = "The name to be given to the broker and other resources, leave emply to have a default name generated"
  type        = string
  default     = null
}

variable "users" {
  description = "Users to be configured in the broker, at least one admin user needs to be configured"

  type = map(object({
    password : string
    groups : list(string)
    console_access : bool
  }))

  default = {
    #    admin = {
    #      password = "..."
    #      groups = ["admin"]
    #      console_access = true
    #    }
    #
    #    some_user = {
    #      password = "..."
    #      groups = []
    #      console_access = false
    #    }
  }
}

variable "deployment_mode" {
  description = "The deployment mode of the broker. Supported: SINGLE_INSTANCE and ACTIVE_STANDBY_MULTI_AZ"
  type        = string
  default     = "SINGLE_INSTANCE"
}

variable "engine_type" {
  description = "The type of broker engine"
  type        = string
  default     = "ACTIVEMQ"
}

variable "engine_version" {
  description = "The version of the broker engine. See the AmazonMQ Broker Engine docs for supported versions"
  type        = string
  default     = "5.15.9"
}

variable "host_instance_type" {
  description = "The broker's instance type. e.g. mq.t2.micro or mq.m4.large"
  type        = string
  default     = "mq.t2.micro"
}

variable "auto_minor_version_upgrade" {
  description = "Enables automatic upgrades to new minor versions for brokers, as Apache releases the versions."
  type        = bool
  default     = true
}

variable "apply_immediately" {
  description = "Specifies whether any broker modifications are applied immediately, or during the next maintenance window."
  type        = bool
  default     = false
}

variable "publicly_accessible" {
  description = "Whether to enable connections from applications outside of the VPC that hosts the broker's subnets."
  type        = bool
  default     = false
}

variable "enable_general_logging" {
  description = "Logging configuration of the broker. Enables general logging via CloudWatch."
  type        = bool
  default     = false
}

variable "enable_audit_logging" {
  description = "Logging configuration of the broker. Enables audit logging. User management action made using JMX or the ActiveMQ Web Console is logged."
  type        = bool
  default     = false
}

variable "maintenance_day_of_week" {
  description = "Maintenance window start time. The day of the week. e.g. MONDAY, TUESDAY, or WEDNESDAY"
  type        = string
}

variable "maintenance_time_of_day" {
  description = "Maintenance window start time. The time, in 24-hour format. e.g. 02:00"
  type        = string
}

variable "maintenance_time_zone" {
  description = "Maintenance window start time. The time zone, UTC by default, in either the Country/City format, or the UTC offset format. e.g. CET"
  type        = string
  default     = "UTC"
}

variable "security_groups" {
  description = "List of security group ids to assign to broker, used when `var.create_broker_security_group = false`"
  type        = list(string)
  default     = []
}

variable "create_broker_security_group" {
  description = "Whether to create a security group for the broker, used when `var.create_broker_security_group = true`"
  type        = bool
  default     = true
}

variable "security_group_ingress_cidr_blocks" {
  description = "CIDR blocks from where ingress traffic to the broker is allowed, used when `var.create_broker_security_group = true`"
  type        = list(string)
  default     = []
}

variable "security_group_ingress_src_security_group_ids" {
  description = "Security Group IDs from where ingress traffic to the broker is allowed, used when `var.create_broker_security_group = true`"
  type        = list(string)
  default     = []
}

variable "security_group_ingress_ports" {
  description = "Ports to where ingress traffic to the broker is allowed, used when `var.create_broker_security_group = true`"
  type        = list(number)
  default     = [5671, 8883, 61614, 61617, 61619]
}
