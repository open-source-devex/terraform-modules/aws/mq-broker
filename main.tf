locals {
  name = var.name != null ? var.name : "${var.project}-${var.environment}-${data.aws_region.current.name}"

  configure_broker_with_xml  = var.broker_xml_configuration != null
  xml_configuration_id       = join("", aws_mq_configuration.this.*.id)
  xml_configuration_revision = join("", aws_mq_configuration.this.*.latest_revision)

  broker_security_groups = var.create_broker_security_group ? [module.broker_sg.this_security_group_id] : var.security_groups

  tags = merge(var.tags, {
    Name = "${local.name}-activemq"
  })
}

data "aws_region" "current" {}

resource "aws_mq_broker" "this" {
  broker_name                = local.name
  deployment_mode            = var.deployment_mode
  engine_type                = var.engine_type
  engine_version             = var.engine_version
  host_instance_type         = var.host_instance_type
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  apply_immediately          = var.apply_immediately
  publicly_accessible        = var.publicly_accessible
  security_groups            = local.broker_security_groups
  subnet_ids                 = var.vpc_subnet_ids

  tags = local.tags

  logs {
    general = var.enable_general_logging
    audit   = var.enable_audit_logging
  }

  maintenance_window_start_time {
    day_of_week = var.maintenance_day_of_week
    time_of_day = var.maintenance_time_of_day
    time_zone   = var.maintenance_time_zone
  }

  dynamic "user" {
    for_each = var.users

    content {
      username       = user.key
      password       = user.value["password"]
      groups         = user.value["groups"]
      console_access = user.value["console_access"]
    }
  }

  dynamic "configuration" {
    // conditional: create block if XML configuration is present
    for_each = local.configure_broker_with_xml ? [1] : []

    content {
      id       = local.xml_configuration_id
      revision = local.xml_configuration_revision
    }
  }
}

resource "aws_mq_configuration" "this" {
  count = local.configure_broker_with_xml ? 1 : 0

  description    = "Configuration for ${local.name}"
  name           = local.name
  engine_type    = var.engine_type
  engine_version = var.engine_version

  data = var.broker_xml_configuration

  tags = local.tags
}
