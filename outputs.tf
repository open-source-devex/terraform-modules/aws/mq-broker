locals {
  broker_instance_endpoints = flatten(aws_mq_broker.this.instances.*.endpoints)
}

output "mq_broker_console_urls" {
  description = "List of ActiveMQ Broker Web Console URLs"
  value       = aws_mq_broker.this.instances.*.console_url
}

output "mq_broker_endpoints" {
  description = "List of ActiveMQ Broker Endpoints"
  value       = aws_mq_broker.this.instances.*.endpoints
}

output "mq_broker_endpoints_openwire" {
  description = "List of ActiveMQ Broker OpenWire Endpoints"
  value       = [for endpoint in local.broker_instance_endpoints : endpoint if length(regexall("^ssl://.*", endpoint)) > 0]
}

output "mq_broker_endpoints_amqp" {
  description = "List of ActiveMQ Broker AMQP Endpoints"
  value       = [for endpoint in local.broker_instance_endpoints : endpoint if length(regexall("^amqp+ssl://.*", endpoint)) > 0]
}

output "mq_broker_endpoints_stomp" {
  description = "List of ActiveMQ Broker Stomp Endpoints"
  value       = [for endpoint in local.broker_instance_endpoints : endpoint if length(regexall("^stomp+ssl://.*", endpoint)) > 0]
}

output "mq_broker_endpoints_mqtt" {
  description = "List of ActiveMQ Broker MQTT Endpoints"
  value       = [for endpoint in local.broker_instance_endpoints : endpoint if length(regexall("^mqtt+ssl://.*", endpoint)) > 0]
}

output "mq_broker_endpoints_wss" {
  description = "List of ActiveMQ Broker WSS Endpoints"
  value       = [for endpoint in local.broker_instance_endpoints : endpoint if length(regexall("^wss://.*", endpoint)) > 0]
}
