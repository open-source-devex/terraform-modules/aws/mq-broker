data "aws_iam_policy_document" "mq_broker_log_publishing_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["mq.amazonaws.com"]
    }

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = ["arn:aws:logs:*:*:log-group:/aws/amazonmq/*"]
  }
}

resource "aws_cloudwatch_log_resource_policy" "mq_broker_log_publishing_policy" {
  policy_document = data.aws_iam_policy_document.mq_broker_log_publishing_policy.json
  policy_name     = local.name
}
